'use strict';
module.exports = {
	username : process.env.USERNAME || '',
	password : process.env.PASSWORD || '',
	DELAY_MILLISECONDS : process.env.DELAY_MILLISECONDS || 6000,
	profiles : [{
		id : 25025320,
		name : 'instagram'
	},{
		id : 460563723,
		name : 'selenagomez'
	},{
		id : 11830955,
		name : 'taylorswift'
	},{
		id : 7719696,
		name : 'arianagrande'
	},{
		id : 247944034,
		name : 'beyonce'
	},{
		id : 18428658,
		name : 'kimkardashian'
	},{
		id : 6860189,
		name : 'justinbieber'
	}]
};