'use strict';
const http = require('./http');
const log = require('log-util');
const config = require('../config');
const async = require('async');

const _unfollow = (json) => {
	return new Promise((resolve,reject)=>{
		let array = config.profiles.map((profile)=>{
			return (next) => {
				http.post({
					url : 'https://www.instagram.com/web/friendships/'+profile.id+'/unfollow/',
					session : json.session,
					form : {}
				})
				.then((json)=>{
					if(json.statusCode != 200){
						log.error('Unfollowing '+profile.name+' -> StatusCode: '+json.statusCode);
						return next();
					}
					log.warn("Unfollowing "+profile.name);
					next();
				})
				.catch(next);
			};
		});
		setTimeout(()=>{
			async.waterfall(array,(err)=>{
				if(err) return reject('Unfollow -> '+err);
				setTimeout(()=>{
					resolve();
				},config.DELAY_MILLISECONDS+Math.floor(Math.random()*3));
			});
		},config.DELAY_MILLISECONDS+Math.floor(Math.random()*3));
	});
};

module.exports = _unfollow;