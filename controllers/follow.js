'use strict';
const http = require('./http');
const log = require('log-util');
const config = require('../config');
const async = require('async');

const _follow = (json) => {
	return new Promise((resolve,reject)=>{
		let array = config.profiles.map((profile)=>{
			return (next) => {
				http.post({
					url : 'https://www.instagram.com/web/friendships/'+profile.id+'/follow/',
					session : json.session,
					form : {}
				})
				.then((json)=>{
					if(json.statusCode != 200){
						log.error('Following '+profile.name+' -> StatusCode: '+json.statusCode);
						return next();
					}
					log.warn("Following "+profile.name);
					next();
				})
				.catch(next);
			};
		});
		async.waterfall(array,(err)=>{
			if(err) return reject('Follow -> '+err);
			resolve();
		});
	});
};

module.exports = _follow;