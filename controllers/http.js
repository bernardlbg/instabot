'use strict';
const request = require('request');
const log = require('log-util');

const _post = (options) => {
	return new Promise((resolve,reject)=>{
		options.headers = {};
		if(options.session ? (options.session.search(/csrftoken=[0-9a-z]{1,};/) > -1) : false){
			options.headers = {
				'x-csrftoken' : options.session.match(/csrftoken=[0-9a-z]{1,};/)[0].split('=')[1].replace(/[^0-9a-z]/g,''),
				'x-instagram-ajax' : 1,
				'x-requested-with' : 'XMLHttpRequest',
				'user-agent' : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
				'accept' : '*/*',
				'accept-encoding' : 'gzip, deflate, br',
				'accept-language' : 'pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4',
				'origin' : 'https://www.instagram.com',
				'referer' : 'https://www.instagram.com/accounts/login/'
			};
		}
		let jar = getCookies(options);
		request.post({
			url : options.url,
			headers : options.headers,
			jar : jar,
			form : options.form
		},(err,response,body)=>{
			if(err) return reject(err);
			resolve({session:jar.getCookieString(options.url),data:body,statusCode:response.statusCode});
		});
	});
};

const getCookies = (args) => {
    if(!args.session)
        return request.jar();
    const session = args.session;
    const url = args.url;
    let jar = request.jar();
    session.split(" ").map((cookie)=>{
        return request.cookie(cookie.replace(";",""));
    }).forEach((cookie)=>{
        jar.setCookie(cookie,url);
    });
    return jar;
};

module.exports.post = _post;