'use strict';
const http = require('./http');
const log = require('log-util');

const _getSession = () => {
	return new Promise((resolve,reject)=>{
		http.post({
			url : 'https://www.instagram.com/ajax/bz'
		})
		.then((json)=>resolve(json))
		.catch((err)=>reject('_getSession -> '+err));
	});
};

const _login = (account) => {
	return new Promise((resolve,reject)=>{
		_getSession()
		.then((json)=>{
			http.post({
				url : 'https://www.instagram.com/accounts/login/ajax/',
				session : json.session,
				form : account
			})
			.then((json)=>{
				json.session = json.session.replace(/csrftoken=[0-9a-z]{1,};\s/,'');
				resolve(json);
			})
			.catch((err)=>{
				log.error('Login ->',err);
				reject(err);
			});
		})
		.catch((err)=>{
			log.error(err);
			reject(err);
		});
	});
};

module.exports = _login;