'use strict';
const login = require('./controllers/login');
const follow = require('./controllers/follow');
const unfollow = require('./controllers/unfollow');
const async = require('async');
const config = require('./config');
const log = require('log-util');
const error = (err) => log.error(err);
login({
	username : config.username,
	password : config.password
})
.then((json)=>{
	if(json.statusCode != 200)
		return log.error('Login failed. StatusCode: '+json.statusCode);
	log.warn("Logged in successfully");
	async.whilst(()=>true,(next)=>{
		follow(json)
		.then(()=>{
			log.warn('Follow finished');
			unfollow(json).then(()=>{
				log.warn('Unfollow finished');
				next();
			},error);
		},error);
	},(err)=>{});
},error);